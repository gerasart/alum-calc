// Исходные данные:
//
//     Длина одной заготовки: 500.0
// Ширина пропила: 10.0
// 200.0 - 2 шт.
// 20.0 - 3 шт.
// 50.0 - 1 шт.
// 500.0 - 3 шт. (2шт.)+(1шт.)
// 430.0 - 1 шт.
//
//     Решение с наименьшим отходом:
//
//     3x [ ~ 500 (x1) ] = 500
//
// 1x [ 430 (x1) , 50 (x1) ] = 500
//
// 1x [ 200 (x2) , 20 (x3) ] = 500
//
// Требуется заготовок: 5
// Общий остаток: 0.0
// Общая длина: 2500
// Полезный выход: 100 %
// Процент остатка: 0.00 %
// Точек разреза: 5


// если ширина 5
// Решение с наименьшим отходом:
//
//     3x [ ~ 500 (x1) ] = 500
//
// 1x [ 430 (x1) , 50 (x1) ] = 490
//
// 1x [ 200 (x2) , 20 (x3) ] = 485
//
// Требуется заготовок: 5
// Общий остаток: 25.0
// Общая длина: 2500
// Полезный выход: 99 %
// Процент остатка: 1.00 %
// Точек разреза: 7


const help = {
    init: function () {
        this.len_g = 1000;
        this.width_g = 20;
        this.obj = [
            // {
            //     len: 500,
            //     count: 2
            // },
            // {
            //     len: 500,
            //     count: 1
            // },
            // {
            //     len: 200,
            //     count: 1
            // },
            // {
            //     len: 50,
            //     count: 1
            // },
            {
                len: 500,
                count: 2
            },
            {
                len: 500,
                count: 1
            },
            {
                len: 200,
                count: 1
            },
            {
                len: 77,
                count: 1
            },
            {
                len: 200,
                count: 1
            },
            {
                len: 50,
                count: 1
            },
            {
                len: 430,
                count: 1
            },
            {
                len: 50,
                count: 1
            },
            {
                len: 50,
                count: 1
            },

        ];
        this.value_equall = [];
        this.value_not_equal = [];
        this.empty_arr = [];
        this.resultArrays = [];
        this.value_merge = [];
        this.calc();
        this.generateMap();
    },
    calc: function () {
        this.value_equall = this.obj.filter((item) => {
            return item.len === this.len_g;
        });

        this.value_not_equal = this.obj.filter((item) => {
            return item.len !== this.len_g;
        });

        this.value_not_equal.forEach(item => {
            this.dublicateObject(item.count, item);
        });
    },
    dublicateObject: function (times, obj) {
        for (let index = 0; index < times; index++) {
            let new_obj = {
                len: obj.len,
                count: 1,
            };
            this.empty_arr.push(new_obj);
        }
    },
    fixDublicate: function (items_obj) {
        let obj = JSON.parse(JSON.stringify(items_obj));
        let new_arr = [];
        obj.forEach((item) => {
            let test = new_arr.find(x => x.len === item.len);
            if (!test) {
                let arr = obj.filter(x => x.len === item.len);
                let count = 0;

                arr.forEach(it => {
                    count += Number(it.count);
                });

                item.count = count;
                new_arr.push(item);
            }
        });

        return new_arr;
    },
    generateMap: function () {
        this.empty_arr.sort((a,b)=> {
           return b.len-a.len;
        });
        this.empty_arr.forEach(item => {
            if (!this.resultArrays.length) {
                this.resultArrays.push([item]);
            } else {
                let newRow = false;
                this.resultArrays.forEach((row, index, arr) => {
                    let sum = row.map(item => item.len + this.width_g).reduce((prev, next) => prev + next);
                    if ((sum + item.len) < this.len_g ) {
                        arr[index].push(item);
                    } else if ((index + 1) === arr.length) {
                        newRow = [item];
                    }
                });

                if (newRow) {
                    this.resultArrays.push(newRow);
                }
            }
        });

        this.value_equall = this.fixDublicate(this.value_equall);
        let new_result = [];
        this.resultArrays.forEach(item=> {
            new_result.push(this.fixDublicate(item))
        });

        this.value_merge = this.value_equall.concat(new_result);
        console.log(this.value_merge);

    }

};

help.init();


// Если поставить общую длину 1000 то все начинает дублироваться
// Решить вопрос с добавлением ширины детали
