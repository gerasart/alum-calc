var maxlength_id = "maxlength=\"120\"";
var max_val = "step=\"0.001\" max=\"99999\" maxlength=\"12\"";
var max_val2 = "step=\"1\" max=\"99999\" maxlength=\"12\"";

$('#smartcut').submit(function(){
    f = this;
    f.submit.disabled = 1;
    $('.print_smct').css("display", "none");
    $('.smct_repost').css("display", "none");
    $('#smartcut_res').html('<br /><center><div style="background-color:lemonchiffon; width:70%; padding:20px; border:1px solid; margin-top:180px"><b>Расчет...</b><br><font style="font-size:15px">(продолжительность может занимать до 1 минуты.)</font></div></center>');
    $('.round1').removeClass('anim_loaded');
    $('.preround').removeClass('anim_loaded');
    scrollToElement(smct_add);
    var form_data = $("#smartcut").serialize();
    var sar = $("#smartcut").serializeArray();
    var name_styles = '<label class="name_label"><input type="text" id="smct_name" name="name" placeholder="Расчет № 1" value style="display: block;width: 100%;margin: auto;margin-bottom: 30px;border: 0;font-size: 28px;color: #505050;box-sizing: border-box;" onchange="smct_name();"></label>';

    $.post( "/smartcut/smartcut.php", form_data, on_success);
    function on_success(data)
    {
        $('#smartcut_res').html(name_styles + data);
        $('.round1').addClass('anim_loaded');
        $('.preround').addClass('anim_loaded');
        $('.print_smct').css("display", "inline-block");
        $('.smct_repost').css("display", "block");
        f.submit.disabled = 0;
    }
    return false;
});

function smct_name() {
    $("#smct_name").attr('value', $("#smct_name").val());
    console.log($("#smct_name").val());
};

function smct_kod() {

    var id_l = [], id_k = [];

    $(".smct_num").each(function(){

        p = $(this).parent();
        l = p.find('input[name="l[]"]').val()*1;
        id = p.find('input[name="id[]"]').val().trim();
        k = p.find('input[name="c[]"]').val()*1;

        if(id_l[l] && id_l[l].length > 0) {
            id_l[l] = id_l[l] + "+" + id + '<i class>(' + k + "шт.)</i>";
        }
        else id_l[l] = id + '<i class>(' + k + "шт.)</i>";
    });

    for (var i in id_l) {
        $(".smct_ish").each(function(){

            if($(this).html() * 1 == i * 1) {
                if(String(id_l[i]).indexOf('+') == -1)
                    id_l[i] = id_l[i].replace(/<i class>/g, '<i style="display:none;">');

                $(this).parent().after(" <i> "+ id_l[i] +"</i>");
            }
        });

        $(".smct_kod").each(function(){
            if($(this).html() * 1 == i * 1) {
                id_l[i] = id_l[i].replace(/<i class>/g, '<i style="display:none;">').replace(/\+/g,'~');
                $(this).before("<i>"+ id_l[i] +" </i>");
            }
        });

    }

}

$('#smct_add').click(function(){
    $(this).parent().parent().before("<tr><td class=\"smct_num\"></td><td><input type=\"number\" name=\"l[]\" "+max_val+" onchange=\"save_smct()\"></td><td><input type=\"number\" name=\"c[]\" "+max_val2+" onchange=\"save_smct()\"></td><td><input type=\"text\" name=\"id[]\" "+maxlength_id+" onchange=\"save_smct()\"></td><td class=\"smct_del\" onclick=\"smct_del(this);return false;\">X</td></tr>");
    smct_renum();
    $('input[name="l[]"]').last().focus();
    return false;
});

$('.smct_del').click(function(){
    smct_del(this);
    return false;
});

$('.smct_all_del').click(function(){

    var isRemove = confirm("Очистить весь список?");
    if (isRemove != false){
        $('input[name="l[]"]').val("");
        $('input[name="c[]"]').val("");
        $('input[name="id[]"]').val("");
        save_smct();
    }

    return false;
});

function smct_del (element) {
    if ($('td.smct_num').size() > 1) {
        $(element).parent().remove();
        smct_renum();
        sumort_re();
    }
    save_smct();
}

function smct_renum() {
    var i = 1;
    $(".smct_num").each(function(){
        $(this).html(i);
        i++;
    });
}

function save_smct() {
    var n,c,p;
    var s = "";
    $(".smct_num").each(function(){
        p = $(this).parent();
        n = p.find('input[name="l[]"]').val();
        c = p.find('input[name="c[]"]').val();
        id = p.find('input[name="id[]"]').val();
        if( n > 0 ){
            s = s + n + "-" + c + "-" + id + ",";
        }
    });

    $.cookie('smct_save', s);
    $.cookie('smct_l', $('input[name="l_izd"]').val());
    $.cookie('smct_tdisk', $('input[name="t_disk"]').val());
}

function load_smtc(){
    if($.cookie('smct_l') != null && $.cookie('smct_l') != "")
    {
        $('input[name="l_izd"]').val($.cookie('smct_l'));
    }

    if($.cookie('smct_tdisk') != null && $.cookie('smct_tdisk') != "")
    {
        $('input[name="t_disk"]').val($.cookie('smct_tdisk'));
    }

    if($.cookie('smct_save') != null && $.cookie('smct_save').length > 3)
    {
        nc = $.cookie('smct_save').split(',');

        $(".smct_num").each(function(){
            $(this).parent().remove();
        });

        var e = $('#smct_add').parent().parent();

        for (var i = 0; i < nc.length; ++i) {
            ar = nc[i].split('-');
            var id_smct = "";
            if( ar[2] ) { id_smct = ar[2] }
            if(ar[0] > 0 && ar[1])
            {
                e.before("<tr><td class=\"smct_num\"></td><td><input type=\"number\" name=\"l[]\" "+max_val+" onchange=\"save_smct()\" value=\""+ar[0]+"\"></td><td><input type=\"number\" name=\"c[]\" "+max_val2+" onchange=\"save_smct()\" value=\""+ar[1]+"\"></td><td><input type=\"text\" name=\"id[]\" "+maxlength_id+" onchange=\"save_smct()\" value=\""+id_smct+"\"></td><td class=\"smct_del\" onclick=\"smct_del(this);return false;\">X</td></tr>");
            }
        }
        smct_renum();
        sumort_re();
    }
}

load_smtc();

$('.smct_import_submit').click(function(){


    if($("#smct_name_import").prop("checked")) var s = $('#smct_textarea').val().replace(/,/g, '.').replace(/[^0-9.A-zА-я]+/ig, "-");
    else var s = $('#smct_textarea').val().replace(/,/g, '.').replace(/[^0-9.]+/ig, "-");

    if(s.length < 4) return false;

    arr = s.split("-");


    $(".smct_num").each(function(){
        $(this).parent().remove();
    });

    var e = $('#smct_add').parent().parent();

    var step = $("#smct_name_import").prop("checked") ? 3 : 2;

    for (var i = 0; i < arr.length; i = i + step) {

        if(arr[i] && arr[i+1])
        {
            var id_str = $("#smct_name_import").prop("checked") ? arr[i+2] : "";
            e.before("<tr><td class=\"smct_num\"></td><td><input type=\"number\" name=\"l[]\" "+max_val+" onchange=\"save_smct()\" value=\""+arr[i]+"\"></td><td><input type=\"number\" name=\"c[]\" "+max_val2+" onchange=\"save_smct()\" value=\""+parseInt(arr[i+1])+"\"></td><td><input type=\"text\" name=\"id[]\" "+maxlength_id+" onchange=\"save_smct()\" value=\""+id_str+"\"></td><td class=\"smct_del\" onclick=\"smct_del(this);return false;\">X</td></tr>");
        }
    }
    smct_renum();
    sumort_re();
    save_smct();
    return false;
});

function scrollToElement(theElement) {
    if (typeof theElement === "string") theElement = document.getElementById(theElement);
    var selectedPosX = 0;
    var selectedPosY = 0;

    while (theElement != null) {
        selectedPosX += theElement.offsetLeft;
        selectedPosY += theElement.offsetTop;
        theElement = theElement.offsetParent;
    }

    window.scrollTo(selectedPosX,selectedPosY);
}

$( "#b_ostatki" ).click(function() {
    $(".smct_ostatki").toggle();
});

$( "#b_map" ).click(function() {
    $("#smct_map").toggle();
});

function smct_map_start2()  {
    ar_smct_map = [];
    ar_smct_shem = [];
    var smct_tochek_pila = 0;
    var vsego_dlina = 0;
    var vsego_ostatok = 0;

    $(".smct_zag").each(function(){

        tr_id = $(this).data("smct_c").substring(0, $(this).data("smct_c").length - 1) * 1;
        if(!(ar_smct_map[tr_id] && ar_smct_map[tr_id].length > 6))
        {
            ar_smct_map[tr_id] = "";
            ar_smct_shem[tr_id] = "";
        }

        ar_smct_map[tr_id] = ar_smct_map[tr_id] + '<table width="100%" cellspacing="0" style="margin-top: -12px; border-collapse: collapse; text-align: center;"><tbody><tr><td style="width=6%; padding: 4px 4px 4px 0; font-size: 16px;">'+$(this).data("smct_c")+'</td>';
        ar_smct_shem[tr_id] = ar_smct_shem[tr_id] + '<p>'+$(this).data("smct_c") + ' [ ';

        all_l = $(this).children(".smct_ostatki").data("smct_all_l") * 1;
        vsego_dlina += all_l * tr_id;

        smct_zag_ost = $(this).children(".smct_ostatki").data("smct_d") * 1;
        vsego_ostatok += smct_zag_ost * tr_id;

        smct_ost = 0; smct_ost_st = ""; smct_ost_shem = "";
        smct_ost = 94 * smct_zag_ost / all_l;
        if(smct_ost > 0)
        {
            smct_ost_shem = '<i class="smct_ostatki">(остаток '+ smct_zag_ost + ' )</i>'
            smct_ost_st = '<td style="width:'+smct_ost+'%; background-color: #eee; padding: 4px 0; border: 1px solid #838383; text-align:center;" class="smct_td_in">'
                +'-<i class="smct_ostatki">ост.'+ smct_zag_ost + '</i>' + '</td>';
        }
        else smct_tochek_pila = smct_tochek_pila - tr_id;

        smct_otr = "";
        shem_otr = "";

        $(this).children(".smct_otr ").each(function(){

            smct_tochek_pila = smct_tochek_pila + $(this).data("smct_k") * tr_id;

            tdwidth = $(this).data("smct_k") * 94 * $(this).data("smct_d") / all_l;

            x_otr = "";
            if ($(this).data("smct_k") > 1) {x_otr = ' (x'+$(this).data("smct_k")+')';}

            smct_otr = '<td style="width:'+tdwidth+'%; background-color: #feffd0; padding: 4px 0; border: 1px solid #838383; text-align:center;"><span class="smct_kod">'+$(this).data("smct_d")*1+'</span>'+x_otr+'</td>';
            shem_otr = '<span class="smct_kod">'+$(this).data("smct_d")*1+'</span>'+' (x'+$(this).data("smct_k")+') , ';

            ar_smct_map[tr_id] = ar_smct_map[tr_id] + smct_otr; //.repeat($(this).data("smct_k"));
            ar_smct_shem[tr_id] = ar_smct_shem[tr_id] + shem_otr;
        });

        ar_smct_map[tr_id] = ar_smct_map[tr_id] + smct_ost_st + '</tr></tbody></table><br>';
        ar_smct_shem[tr_id] = ar_smct_shem[tr_id].substring(0, ar_smct_shem[tr_id].length - 2) + '] = '+ (all_l-$(this).children(".smct_ostatki").data("smct_d") * 1) + " " + smct_ost_shem + '</p>';
    });

    //alert(vsego_ostatok);

    for (var i in ar_smct_map) {
        $("#smct_map").prepend(ar_smct_map[i]);
    }
    $("#smct_map").prepend('<center><b>Карта раскроя:</b></center><br>');

    for (var i in ar_smct_shem) {
        $(".smct_sheme").prepend(ar_smct_shem[i]);
    }

    procent_ost = vsego_ostatok * 100 / vsego_dlina;
    $(".smct_sourse").before();
    $(".smct_sourse").before(
        'Общая длина: ' + vsego_dlina + '<br>' +
        'Полезный выход: ' + (100 - procent_ost.toFixed(2)) + ' %<br>' +
        'Процент остатка: ' + procent_ost.toFixed(2) + ' %<br>');
    $(".smct_sourse").before('Точек разреза: ' + smct_tochek_pila + '<br>');
}

$('#b_print').click(function(){
    var html_to_print=$('#smartcut_res').html();
    var iframe=$('<iframe id="print_frame">');
    $('body').append(iframe);
    var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
    var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
    doc.getElementsByTagName('body')[0].innerHTML=html_to_print;
    win.print();
    $('#print_frame').remove();
});

$('#smartcut').bind("change keyup", 'input[name="c[]"]' , function(){
    sumort_re();
});

function sumort_re()  {
    var sumotr = 0;
    $('input[name="c[]"]').each(function(){
        sumotr = sumotr + $(this).val()*1
    });
    $('#smct_sumotr').html(sumotr > 0 ? "("+sumotr+")" : "");
}